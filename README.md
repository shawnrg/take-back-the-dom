# Take Back The Dom

Blacklist specific elements from being rendered in the DOM for some popular sites.

## Motivation

[Studies](https://www.psychologytoday.com/us/blog/obesely-speaking/201710/social-media-is-harmful-your-brain-and-relationships) have shown that social media can be incredibly damaging to the brain given that it is often designed to be addictive. There are merits to social media but they have been drowned out by the excessive need to keep users engaged so that they stay on the site longer and are able to be served more ads. This Chrome extension should in theory help bring those merits back by hiding the intentionally addicting parts of social media from the user.


## How It Works

No magic. The extension loads a content script into the page when visiting Facebook, initializes a mutation observer on the document body and watches for things we don't want to be added to the DOM. When added, we simply set their `display` to `None`. 
