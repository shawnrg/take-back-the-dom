const elementSelectors = [
  '[aria-label="News Feed"]',
  '[id="pagelet_ego_pane"]',
  '[id="stories_pagelet_rhc"]'
];

// Select the node that will be observed for mutations
//const targetNode = document.getElementById("globalContainer");
const targetNode = document.body;

// Options for the observer (which mutations to observe)
const config = { attributes: true, childList: true, subtree: true };

const callback = function(mutationsList, observer) {
  for (let mutation of mutationsList) {
    if (mutation.addedNodes.length) {
      for (let selector of elementSelectors) {
        const found = mutation.target.querySelector(selector);

        if (found) {
          found.style.display = "None";
        }
      }
    }
  }
};

// Create an observer instance linked to the callback function
const observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
observer.observe(targetNode, config);
